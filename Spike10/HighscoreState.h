#pragma once

#include "State.h"

class HighscoreState : public State
{
public:
	HighscoreState(std::vector<std::string> ids);
	~HighscoreState();

	std::string GetInput();

	void Handle(Command* cmd);

	void Display();
};

