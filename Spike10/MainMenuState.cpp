#include "MainMenuState.h"

using namespace std;

/*
	The main menu state, also the starting state for the programs. Facilitates the user switching states

	@param ids ids for this state
*/
MainMenuState::MainMenuState(vector<string> ids)
: State(ids)
{
}

/*
	Handles the command passed in. Does nothing (this state is used for state switching, handled by the context controller)
*/
void MainMenuState::Handle(Command* cmd)
{
	
}

/*
	Gets the users input, and modifies it slightly so that the command processor knows where it is from

	@returns string modified unformatted input
*/
string MainMenuState::GetInput()
{
	string result = "MAINMENU ";
	string input;
	getline(cin, input);
	result += input;

	return result;
}

/*
	Prints to console this states information (what menu options exist
*/
void MainMenuState::Display()
{
	cout << "================================================" << endl;
	cout << "What would you like to do?" << endl;
	cout << endl;
	cout << "\t[START] Pick an adventure and go!" << endl;
	cout << "\t[HOF] Visit the hall of fame" << endl;
	cout << "\t[ABOUT] Learn about the game" << endl;
	cout << "\t[HELP] Accessible any time" << endl;
	cout << "\t[QUIT] Quit the program" << endl;

}

MainMenuState::~MainMenuState()
{
}

