#pragma once

#include "StateCommand.h"

class StartCommand : public StateCommand
{
public:
	StartCommand(std::vector<std::string> ids);
	void Update(std::vector<std::string> input);

	std::string Adventure();

	~StartCommand();

private:
	std::string _adventure;
};

