#include "WeaponComponent.h"

using namespace std;


/*
	Weapon component, inherits from component. Indicates the parent entity can be used as a weapon. Has a chance to hit and a int damage

	@param chance to hit
	@param damage how much damage this weapon does
*/
WeaponComponent::WeaponComponent(int chance, int damage)
:Component({"WEAPON"})
{
	_chance = chance;
	_damage = damage;
}

/*
	@returns string description of this component
*/
string WeaponComponent::CompDesc()
{ 
	return "\n\tWeapon";
}


WeaponComponent::~WeaponComponent()
{
}
