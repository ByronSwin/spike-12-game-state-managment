#pragma once

#include "Command.h"

class QuitCommand : public Command
{
public:
	QuitCommand(std::vector<std::string> ids);
	~QuitCommand();

};

