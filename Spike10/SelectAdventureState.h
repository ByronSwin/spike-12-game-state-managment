#pragma once

#include "State.h"
#include "StartCommand.h"
#include "GameData.h"
#include "Loader.h"

#include <vector>
#include <dirent.h>
#include <algorithm>

class SelectAdventureState : public State
{
public:
	SelectAdventureState(std::vector<std::string> ids, GameData* theGame);
	~SelectAdventureState();

	std::string GetInput();

	void Handle(Command* cmd);

	void Display();

private:

	std::vector<std::string> _adventures;
	GameData* _theGame;
	bool _loaded;
};

