#include "Player.h"
#include "InventoryComponent.h"

using namespace std;

/*
	The player class, currently has hard-coded identifiers, names and descriptions. Inherits from entity, but has a location
*/
Player::Player()
: Entity(vector<string>({ "ME" , "PLAYER"}), "A nameless figure, you.", "You have no memory of who you are or how you got here")
{
	AddComponent(new InventoryComponent());
}

/*
	Sets the current location of this player to the parameter
	@param newLoc Location pointer to set to.
*/
void Player::SetLocation(Location* newLoc)
{
	_currLocation = newLoc;
}

/*
	@returns location the current location of the player
*/
Location* Player::GetLocation()
{
	return _currLocation;
}

/*
	@returns the long description of this player, including all the items in it's inventory
*/
string Player::LongDesc()
{
	string result = "\n" + _name + "\t" + FirstId() + "\n";
	result += Desc();
	map <string, Component*>::iterator it;

	result += "\nYou have:";
	for (it = _components.begin(); it != _components.end(); it++)
	{
		result += it->second->CompDesc();
	}

	return result;
}


Player::~Player()
{
}
