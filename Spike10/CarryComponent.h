#pragma once

#include "Component.h"


class CarryComponent : public Component
{
public:
	CarryComponent();
	std::string CompDesc();
	~CarryComponent();
};

