
#pragma once

#include "Component.h"


class Entity;

class InventoryComponent : public Component
{
public:
	InventoryComponent();
	~InventoryComponent();

	void AddEntity(Entity* toAdd);
	Entity* TakeEntity(std::string toTake);
	Entity* GetEntity(std::string toGet);
	bool HasEntity(std::string toCheck);

	std::vector<Entity*> GetInv();

	std::string CompDesc();


private:
	std::vector<Entity*> _inventory;
};

