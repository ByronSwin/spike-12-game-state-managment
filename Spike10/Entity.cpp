#include "Entity.h"

#include "Component.h"


using namespace std;


/*
	Entity is the main game object for this game. Inherits from IObject, using their id system. Entities also have 
	a name and a description.

	In every entity's constructor, it subscribes to annoucements for the message system so that it can recieve relevant messages

	Entities have a vector of components that give each entity traits (carryable, has an inventory etc.

	@param ids identifiers for this entity
	@param name the name for this entity
	@param desc the description for this entity
*/
Entity::Entity(vector<string> ids,string name, string desc)
: IObject(ids)
{
	//_components = map<string, Component*>();
	_name = name;
	_desc = desc;

	MessageSystem::Subscribe(this, "ANNOUCEMENT");
	
}

/*
	Adds a component to this entity
*/
void Entity::AddComponent(Component* newComponent)
{
	_components[newComponent->FirstId()] = newComponent;
}

/*
	Removes a component from this entity (currently unused)
*/
void Entity::RemoveComponent(Component* toRem)
{
	_components[toRem->FirstId()] = NULL;
}

/*
	Checks if this entity has a component with the matching id
	@param compId string identifier for the component

	@returns true if has this component, else false
*/
bool Entity::HasComponent(string compId)
{
	string compName = "(";
	compName += compId + ")";
	if (_components.count(compName) == 1)
	{
		return true;
	}
	else
	{
		return false;
	}
}

/*
	Returns a pointer to a component if any match the identifier

	@param toGet string identifer of the component
	@returns pointer to the component
*/
Component* Entity::GetComponent(string toGet)
{
	string compName = "(";
	compName += toGet + ")";

	if (_components.count(compName) == 1)
	{

		return _components[compName];
	}
	else
	{
		return NULL;
	}
}

/*
	Handles the recieveing of a message for this entity. Passes it on to all components, as the message may be for a specific component of this entity
*/
void Entity::Recieve(Message* toRec)
{
	map<string, Component*>::iterator it;
	for (it = _components.begin(); it != _components.end(); it++)
	{
		it->second->Recieve(toRec);
	}
}

/*
	Returns the long description of this entity (which includes its name, its primary identifier and its description.
	Also adds the description of all connected components.
*/
string Entity::LongDesc()
{
	string result = "\n" + _name + "\t" + FirstId();
	result += "\n" + _desc;
	map <string, Component*>::iterator it;
	for (it = _components.begin(); it != _components.end(); it++)
	{
		result += it->second->CompDesc();
	}

	return result;
}

/*
	@returns string name of this entity
*/
string Entity::Name()
{
	return _name;
}

/*
	@returns string description of this entity
*/
string Entity::Desc()
{
	return _desc;
}

Entity::~Entity()
{
	_components.clear();
}