#pragma once

#include "Entity.h"
#include "Location.h"



class Player : public Entity
{
public:
	Player();
	~Player();

	virtual std::string LongDesc();

	void SetLocation(Location* newLoc);
	Location* GetLocation();
private:
	Location* _currLocation;
};

