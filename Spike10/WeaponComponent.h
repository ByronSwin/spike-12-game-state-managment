#pragma once

#include "Component.h"

class WeaponComponent : public Component
{
public:
	WeaponComponent(int chance, int damage); 
	std::string CompDesc();
	~WeaponComponent();
private:
	int _chance;
	int _damage;
};

