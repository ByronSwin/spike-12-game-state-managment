#include "SelectAdventureState.h"

using namespace std;

/*
	The select adventure state. This handles both the selecting of an adventure by the user as well as the loading of the game data.
	Takes in a pointer to the current game data, and uses a boolean to check if it has been fully loaded

	It reads all the files in target directory to list as possible adventures

	KNOWN BUG: There is a file titled . and .. but no file exists. Not sure what is causing it, but low priority

	@param ids the ids for this state
	@param theGame pointer to game data for the game
*/
SelectAdventureState::SelectAdventureState(vector<string> ids, GameData* theGame)
: State(ids)
{

	_theGame = theGame;
	_loaded = false;

	DIR *dir;
	struct dirent *ent;

	if ((dir = opendir("Adventures\\")) != NULL)
	{
		while ((ent = readdir(dir)) != NULL)
		{
			_adventures.push_back(ent->d_name);
		}
		closedir(dir);
	}
	else
	{
		perror("");
		cout << "went wrong" << endl;
	}
}

/*
	Gets the user input if the adventure has not been loaded, otherwise, tells command processor to start the game
*/
string SelectAdventureState::GetInput()
{
	string result = "ADVENTURESELECT ";
	if (!_loaded)
	{
		string input;
		getline(cin, input);
		result += input;
	}
	else
	{
		result += "STARTTHEGAME"; //technically this is doably by a user. It's an unlikely string that the user enters, so its a low priority to fix.
	}

	return result;
}


/*
	Handles the command. If its a start command and it targets an adventure that exists, that file is used to load into the game data pointer
*/
void SelectAdventureState::Handle(Command* cmd)
{
	if (typeid(*cmd) == typeid(StartCommand))
	{
		StartCommand* c = dynamic_cast<StartCommand*>(cmd);
		for (string s : _adventures)
		{
			string test = s;
			transform(test.begin(), test.end(), test.begin(), ::toupper);
			if (test.compare(c->Adventure()) == 0)
			{
				Loader gameLoader(_adventures);
				gameLoader.Load(s, _theGame);
				_theGame->StartGame();
				_loaded = true;
				break;
			}
		}
	}
}

/*
	Displays to console this states information. Prints all the adventures the user can choose from
*/
void SelectAdventureState::Display()
{
	if (!_loaded)
	{
		cout << "================================================" << endl;
		cout << "Choose an adventure: " << endl;
		cout << "[BACK] to go back" << endl;

		for (string s : _adventures)
		{
			cout << s << endl;
		}
		cout << endl << "[BACK] to go back" << endl;
	}
	else
	{
		cout << endl << "Adventure loaded!" << endl;
	}
	cout << "================================================" << endl;
}

SelectAdventureState::~SelectAdventureState()
{
	_theGame = NULL;
}