#include "LockComponent.h"

using namespace std;

/*
	Lock component, inherits from component. This component is used to lock paths based on idenfitifiers. If a message is delievered to unlock, and it has a matching
	lock val, the component is unlocked

	@param lockVal the lock value
	@param pathVal identifier for the path/direction that is locked.
*/
LockComponent::LockComponent(string lockVal, string pathVal)
: Component({ "LOCK" })
{
	_locked = true;
	_lockVal = lockVal;
	_pathVal = pathVal;
}

/*
	An override for recieve that checks if the message is an unlock message. If it is, checks if data matches. If it does, unlocks

	@param toRec the message recieved
*/
void LockComponent::Recieve(Message* toRec)
{
	if (toRec->Subject().compare("UNLOCK") == 0)
	{
		Unlock(toRec->Data());
	}
}

/*
	Checks if the string matches the lock val. If it matches, unlock
	
	@param keyVal the string to check against the lock val.
*/
void LockComponent::Unlock(string keyVal)
{
	if (keyVal.compare(_lockVal) == 0)
	{
		_locked = false;
	}
}


/*
	Returns the description of this component
	@returns the description
*/
string LockComponent::CompDesc()
{
	string result = "\n\tLock";
	return result;
}

/*
	Returns the path identifier this lock is for
	@returns path identifier
*/
string LockComponent::GetPathVal()
{
	return _pathVal;
}

/*
	Returns where the lock is currently locked
	@returns bool the value of _locked
*/
bool LockComponent::isLocked()
{
	return _locked;
}

LockComponent::~LockComponent()
{
}
